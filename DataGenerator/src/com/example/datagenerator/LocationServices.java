package com.example.datagenerator;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

public class LocationServices {

	private final static int ACCURACY = 4000;
	private final static String TAG = "Gen";

	/**
	 * try to get the 'best' location selected from all providers
	 */
	public static Location getBestLocation(Context c, int interval) {
		Location gpslocation = getLocationByProvider(
				LocationManager.GPS_PROVIDER, c);
		Location networkLocation = getLocationByProvider(
				LocationManager.NETWORK_PROVIDER, c);
		// if we have only one location available, the choice is easy
		if (gpslocation == null) {
			Log.d(TAG, "No GPS Location available.");
			if (networkLocation != null
					&& networkLocation.getAccuracy() < ACCURACY) {
				Log.d(TAG, "Available accurate network location");
				return networkLocation;

			} else {
				Log.d(TAG, "No Network Location available");
				return null;
			}
		}
		if (networkLocation == null) {
			Log.d(TAG, "No Network Location available");
			return gpslocation;
		}
		boolean gpsIsOld = oldLocation(gpslocation, interval);
		boolean networkIsOld = oldLocation(networkLocation, interval);
		// gps is current and available, gps is better than network
		if (!gpsIsOld) {
			return gpslocation;
		}
		// gps is old, we can't trust it. use network location
		if (!networkIsOld) {
			Log.d(TAG, "GPS is old, Network is current, returning network");
			return networkLocation;
		}
		// both are old return the newer of those two
		if (gpslocation.getTime() > networkLocation.getTime()) {
			Log.d(TAG, "Both are old, returning gps(newer)");
			return gpslocation;
		} else {
			Log.d(TAG, "Both are old, returning network(newer)");
			return networkLocation;
		}
	}

	private static boolean oldLocation(Location l, int interval) {
		long old = System.currentTimeMillis() - interval;
		return (l.getTime() < old);
	}

	/**
	 * get the last known location from a specific provider (network/gps)
	 */
	private static Location getLocationByProvider(String provider, Context c) {
		Location location = null;

		LocationManager locationManager = (LocationManager) c
				.getApplicationContext().getSystemService(
						Context.LOCATION_SERVICE);
		try {
			if (locationManager.isProviderEnabled(provider)) {
				location = locationManager.getLastKnownLocation(provider);
			}
		} catch (IllegalArgumentException e) {
			Log.d("Gen", "Cannot acces Provider " + provider);
		}
		return location;
	}

}
